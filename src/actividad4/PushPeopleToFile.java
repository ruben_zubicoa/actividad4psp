/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad4;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zubicoa
 */
public class PushPeopleToFile implements Runnable {

    private int numberOfPeople;

    /**
     * Class constructor.
     *
     * @param numberOfPeople people to insert.
     */
    public PushPeopleToFile(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public void run() {
        System.out.println("Starting to push people to a text file");
//TODO: Crea los objetos necesarios para escribir a fichero en Java. 
//A continuación, dentro de un bucle con el mismo número de iteraciones
//que personas a insertar, crea una espera
//de medio segundo, crea la persona y vuélcala a fichero 

        FileWriter fichero = null;
        PrintWriter pw = null;

      

        try {
            fichero = new FileWriter("personas.txt");
            pw = new PrintWriter(fichero);

            for (int i = 0; i < this.numberOfPeople; i++) {
                pw.println(new Person().toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                fichero.close();
            } catch (IOException ex) {
                Logger.getLogger(PushPeopleToFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("Everyone is already in a text file");

    }
}
