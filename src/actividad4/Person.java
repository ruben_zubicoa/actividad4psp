/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad4;

import java.util.Random;

/**
 *
 * @author Zubicoa
 */
public class Person {

    //TODO: Declare the attributes of a person: age and dni
    private Integer age;
    private String dni;

    /**
     * Class constructor.
     */
    public Person() {
//Initialize attributes to random values.The age must be an integer
//between 0 and 100, and the dni a 8 characters alphanumeric string

        Random rand = new Random();

        this.age = rand.nextInt((100)) + 0;

        int num = rand.nextInt((99999999)) + 10000000;

        String juegoCaracteres = "TRWAGMYFPDXBNJZSQVHLCKE";
        int modulo = num % 23;
        char letra = juegoCaracteres.charAt(modulo);

        this.dni = num + "-" + letra;

    }

    @Override
    public String toString() {
//TODO: Return a string with the attributes of a Person to be 
//printed in the file

        return "DNI : " + this.dni + " Age : " + this.age;

    }
}
