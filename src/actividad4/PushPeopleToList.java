/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad4;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Zubicoa
 */
public class PushPeopleToList implements Runnable {

    /**
     * Number of people to insert.
     */
    private int numberOfPeople;

    /**
     * Class constructor.
     *
     * @param numberOfPeople people to insert.
     */
    public PushPeopleToList(int numberOfPeople) {
        this.numberOfPeople = numberOfPeople;
    }

    @Override
    public void run() {
        System.out.println("Starting to push people to an ArrayList");
//TODO: Crea un ArrayList. A continuación, dentro de un bucle con el 
//mismo número de iteraciones que personas a insertar,crea una espera 
//de medio segundo, crea la persona a insertar e insértala 

        int c = 0;

        List<Person> personas = new ArrayList<Person>();

        while (c != this.numberOfPeople) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(PushPeopleToList.class.getName()).log(Level.SEVERE, null, ex);
            }

            Person p = new Person();

            personas.add(p);

            System.out.println("Se ha añadido " + p.toString());

            c++;
        }

        System.out.println("Everyone is already in the ArrayList");
    }
}
